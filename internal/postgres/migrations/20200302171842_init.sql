-- +goose Up
-- +goose StatementBegin
create table anagrams (
    id bigserial primary key,
    hash text not null,
    word text not null unique
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table anagrams;
-- +goose StatementEnd
