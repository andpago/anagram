package postgres

import (
	"anagram/internal/logger"
	"anagram/internal/service"
	"context"
	"database/sql"

	"github.com/pkg/errors"
)

func expectError(ctx context.Context, action func() error, msg string) {
	err := action()
	if err != nil {
		logger.FromContext(ctx).WithError(err).Error(msg)
	}
}

func rollbackTx(ctx context.Context, tx *sql.Tx, allowDone bool) {
	err := tx.Rollback()
	if err == sql.ErrTxDone && allowDone {
		return
	}

	if err != nil {
		logger.FromContext(ctx).WithError(err).Error("failed to rollback tx")
	}
}

func (p *Postgres) SaveWords(ctx context.Context, words []service.HashedWord) (resErr error) {
	tx, err := p.DB.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelReadCommitted,
		ReadOnly:  false,
	})
	if err != nil {
		return errors.Wrap(err, "failed to begin tx")
	}

	defer func() {
		rollbackTx(ctx, tx, resErr == nil)
	}()

	_, err = tx.ExecContext(ctx, `delete from anagrams`)
	if err != nil {
		return errors.Wrap(err, "failed to delete old anagrams")
	}

	stmt, err := tx.PrepareContext(ctx,
		`insert into anagrams(hash, word) values ($1, $2) on conflict(word) do nothing`)
	if err != nil {
		return errors.Wrap(err, "failed to create stmt")
	}
	defer expectError(ctx, stmt.Close, "failed to close statement")

	for _, word := range words {
		_, err = stmt.ExecContext(ctx, word.Hash, word.Word)
		if err != nil {
			return errors.Wrap(err, "failed to insert")
		}
	}

	return errors.Wrap(tx.Commit(), "failed to commit")
}
