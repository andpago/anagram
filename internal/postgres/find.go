package postgres

import (
	"context"

	"github.com/pkg/errors"
)

func (p *Postgres) FindWordsWithHash(ctx context.Context, hash string) ([]string, error) {
	rows, err := p.DB.QueryContext(ctx, `select word from anagrams where hash = $1 order by id asc`, hash)
	if err != nil {
		return nil, errors.Wrap(err, "failed to query")
	}
	defer rows.Close()

	var words []string

	for rows.Next() {
		var next string

		err := rows.Scan(&next)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan word")
		}

		words = append(words, next)
	}

	return words, nil
}
