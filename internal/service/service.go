package service

import (
	"context"

	"github.com/pkg/errors"
)

type HashedWord struct {
	Word, Hash string
}

type Repo interface {
	SaveWords(ctx context.Context, words []HashedWord) error
	FindWordsWithHash(ctx context.Context, hash string) ([]string, error)
}

type Service struct {
	Repo Repo
}

func (s *Service) SaveWords(ctx context.Context, words []string) error {
	hashedWords := make([]HashedWord, 0, len(words))
	for _, word := range words {
		hashedWords = append(hashedWords, HashedWord{
			Word: word,
			Hash: anagramHash(word),
		})
	}

	return errors.Wrap(s.Repo.SaveWords(ctx, hashedWords), "failed to save words")
}

func (s *Service) FindAnagrams(ctx context.Context, word string) ([]string, error) {
	hash := anagramHash(word)

	words, err := s.Repo.FindWordsWithHash(ctx, hash)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to find words with hash %s", hash)
	}

	return words, nil
}
