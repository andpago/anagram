package service

import (
	"sort"
	"strings"
)

func anagramHash(word string) string {
	runes := []rune(strings.ToLower(word))
	sort.Slice(runes, func(i, j int) bool {
		return runes[i] < runes[j]
	})

	return string(runes)
}
