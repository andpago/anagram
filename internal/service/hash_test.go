package service

import "testing"

func TestAnagramHash_Equal(t *testing.T) {
	hash1 := anagramHash("hello world")
	hash2 := anagramHash("oldwr lloeh")

	if hash1 != hash2 {
		t.Errorf("hash1 (%s) != hash2 (%s)", hash1, hash2)
	}
}

func TestAnagramHash_ExtraLetters(t *testing.T) {
	hash1 := anagramHash("hello world")
	hash2 := anagramHash("hello world hello world")

	if hash1 == hash2 {
		t.Errorf("hash1 (%s) = hash2 (%s)", hash1, hash2)
	}
}

func TestAnagramHash_Different(t *testing.T) {
	hash1 := anagramHash("hello world")
	hash2 := anagramHash("lorem ipsum")

	if hash1 == hash2 {
		t.Errorf("hash1 (%s) = hash2 (%s)", hash1, hash2)
	}
}
