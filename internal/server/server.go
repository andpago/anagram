package server

import (
	"anagram/internal/logger"
	"anagram/internal/service"
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/pkg/errors"

	"github.com/go-chi/chi"
)

const (
	gracefulShutdownTimeout = 5 * time.Second
)

type Server struct {
	service   *service.Service
	timeout   time.Duration
	rateLimit int
}

func FromService(svc *service.Service, rateLimit int, timeout time.Duration) Server {
	return Server{service: svc, rateLimit: rateLimit, timeout: timeout}
}

// error responds to the client with an appropriate error and message.
// It also logs non-nil errors for 5xx response codes
func (s *Server) error(ctx context.Context, w http.ResponseWriter, code int, msg string, err error) {
	log := logger.FromContext(ctx)

	if errors.Cause(err) == context.DeadlineExceeded && code == http.StatusInternalServerError {
		code = http.StatusRequestTimeout
	}

	if code >= http.StatusInternalServerError && err != nil {
		log.WithError(err).Error("5xx error")
	}

	w.WriteHeader(code)

	type resp struct {
		Error string `json:"error"`
	}

	err = json.NewEncoder(w).Encode(msg)
	if err != nil {
		log.WithError(err).WithField("msg", resp{Error: msg}).
			Error("failed to encode msg to response")
	}
}

func (s *Server) success(ctx context.Context, w http.ResponseWriter, msg interface{}) {
	w.WriteHeader(http.StatusOK)

	err := json.NewEncoder(w).Encode(msg)
	if err != nil {
		logger.FromContext(ctx).
			WithError(err).
			WithField("msg", msg).
			Error("failed to encode msg to response")
	}
}

func (s *Server) asHandler(log logger.Logger) http.Handler {
	mux := chi.NewRouter()

	mux.Use(s.rateLimitMiddleware(s.rateLimit), loggingMiddleware(log), timeoutMiddleware(s.timeout))

	mux.Post("/load", s.HandleLoadWords)
	mux.Get("/get", s.HandleFindAnagrams)

	return mux
}

func (s *Server) Listen(ctx context.Context, address string) error {
	srv := http.Server{
		Handler: s.asHandler(logger.FromContext(ctx)),
		Addr:    address,
	}

	errChan := make(chan error, 1)

	go func() {
		errChan <- srv.ListenAndServe()
	}()

	select {
	case err := <-errChan:
		return err
	case <-ctx.Done():
		shutdownCtx, cancel := context.WithTimeout(context.Background(), gracefulShutdownTimeout)
		defer cancel()

		err := srv.Shutdown(shutdownCtx)
		if err != nil {
			return err
		}

		return ctx.Err()
	}
}
