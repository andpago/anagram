package server

import (
	"context"
	"net/http"
	"time"

	"go.uber.org/ratelimit"
)

func noopMiddleware(handler http.Handler) http.Handler {
	return handler
}

func (s *Server) rateLimitMiddleware(limit int) func(http.Handler) http.Handler {
	if limit <= 0 {
		return noopMiddleware
	}

	limiter := ratelimit.New(limit)

	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			limiter.Take()
			handler.ServeHTTP(w, r)
		})
	}
}

func timeoutMiddleware(timeout time.Duration) func(handler http.Handler) http.Handler {
	if timeout <= 0 {
		return noopMiddleware
	}

	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx, cancel := context.WithTimeout(r.Context(), timeout)
			defer cancel()

			handler.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
