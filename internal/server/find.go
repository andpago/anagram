package server

import "net/http"

func (s *Server) HandleFindAnagrams(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	word := r.URL.Query().Get("word")
	if word == "" {
		s.error(ctx, w, http.StatusBadRequest, "word query param is mandatory", nil)
		return
	}

	// there are no words larger than maxWordLength in db
	if len(word) > maxWordLength {
		s.success(ctx, w, nil)
		return
	}

	anagrams, err := s.service.FindAnagrams(ctx, word)
	if err != nil {
		s.error(ctx, w, http.StatusInternalServerError, "failed to find anagrams", err)
		return
	}

	if len(anagrams) == 0 {
		anagrams = nil // just in case it is []string{}
	}

	s.success(ctx, w, anagrams)
}
