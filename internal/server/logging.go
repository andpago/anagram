package server

import (
	"anagram/internal/logger"
	"net/http"

	"github.com/sirupsen/logrus"
)

func loggingMiddleware(log logger.Logger) func(handler http.Handler) http.Handler {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.WithFields(logrus.Fields{
				"method":      r.Method,
				"path":        r.URL.Path,
				"remote addr": r.RemoteAddr,
			}).Info()
			handler.ServeHTTP(w, r.WithContext(logger.WithLogger(r.Context(), log)))
		})
	}
}
