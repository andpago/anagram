package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"unicode/utf8"
)

const (
	maxWordLength = 1024    // bytes
	maxBodySize   = 1 << 20 // 1 MiB
)

func (s *Server) HandleLoadWords(w http.ResponseWriter, r *http.Request) {
	var (
		ctx   = r.Context()
		words []string
	)

	if r.ContentLength > maxBodySize {
		s.error(ctx, w, http.StatusRequestEntityTooLarge, fmt.Sprintf("max request size is %v bytes", maxBodySize), nil)
		return
	}

	err := json.NewDecoder(r.Body).Decode(&words)
	if err != nil {
		s.error(ctx, w, http.StatusBadRequest, "failed to decode request", err)
		return
	}

	for _, word := range words {
		if !utf8.ValidString(word) {
			s.error(ctx, w, http.StatusBadRequest, fmt.Sprintf("word '%s' is not a valid unicode string", word), nil)
			return
		}

		if len(word) > maxWordLength {
			s.error(ctx, w, http.StatusBadRequest,
				fmt.Sprintf("one of the words is too long, max length is %v bytes", maxWordLength), nil)
			return
		}
	}

	err = s.service.SaveWords(r.Context(), words)
	if err != nil {
		s.error(ctx, w, http.StatusInternalServerError, "failed to save words", err)
		return
	}

	w.WriteHeader(http.StatusOK)
}
