# Anagram

Сервис поиска анаграмм в словаре.

Адрес на Heroku: https://anagrams-test-task.herokuapp.com/ 

## API

1. `POST /load`
    
    Тело запроса: список строк в json.
    Максимальный размер тела -- 1 MiB, максимальный размер каждого слова 1 KiB.
    
    Например:
    
    ```shell script
    curl -d '["lol", "kek"]' 'http://service/load'
    ```
    
    Ответ: код 200 и пустое тело, если всё ок

2. `GET /get?word=<слово>`

    ```shell script
    curl 'http://service/get?word=kke'
    ```
    
    Ответ: список найденных анаграмм в json:
    
    ```json
    ["kek"]
    ```
   
## Настройки

Сервис настраивается через переменные окружения:

* `ANAGRAM_POSTGRES_DSN` -- dsn к базе данных (postgres)
* `ANAGRAM_PORT` -- порт, на котором слушать http
* `ANAGRAM_RATE_LIMIT` -- максимальный rps, по умолачнию не ограничен
* `ANAGRAM_REQUEST_TIMEOUT` -- таймаут на 1 запрос, по умолчанию таймаута нет

## Ограничения на Heroku

* 100 RPS
* Таймаут 1 секунда