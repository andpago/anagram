module anagram

go 1.14

require (
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.4.2
	go.uber.org/ratelimit v0.1.0
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d // indirect
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
	golang.org/x/text v0.3.2 // indirect
)
