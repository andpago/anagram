package main

import (
	"anagram/internal/logger"
	"anagram/internal/postgres"
	"anagram/internal/server"
	"anagram/internal/service"
	"context"
	"database/sql"
	"fmt"
	"os"
	"time"

	"golang.org/x/sync/errgroup"

	_ "github.com/jackc/pgx/stdlib"
	"github.com/sirupsen/logrus"
)

func main() {
	log := logrus.NewEntry(&logrus.Logger{
		Out: os.Stdout,
		Formatter: &logrus.TextFormatter{
			TimestampFormat: time.RFC822,
			FullTimestamp:   true,
		},
		Level: logrus.InfoLevel,
	})

	config := readConfig(log)
	log.WithField("config", config).Info("loaded config")

	db, err := sql.Open("pgx", config.PostgresDSN)
	if err != nil {
		log.WithError(err).Fatal("failed to connect to db")
	}

	repo := &postgres.Postgres{DB: db}
	svc := &service.Service{Repo: repo}

	srv := server.FromService(svc, config.RateLimit, config.RequestTimeout)

	address := fmt.Sprintf("0.0.0.0:%v", config.Port)
	log.WithField("address", address).Info("serving http")

	ctx := logger.WithLogger(context.Background(), log)
	eg, ctx := errgroup.WithContext(ctx)

	eg.Go(sigTrap(ctx))
	eg.Go(func() error {
		return srv.Listen(ctx, address)
	})

	err = eg.Wait()
	if errSig, ok := err.(errSignal); ok {
		log.Info(errSig.Error())
		return
	}

	log.Fatal(err)
}
