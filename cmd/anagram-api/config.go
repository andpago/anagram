package main

import (
	"os"
	"strconv"
	"time"

	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
)

type Config struct {
	PostgresDSN    string        `envconfig:"POSTGRES_DSN"`
	Port           int           `envconfig:"PORT"`
	RateLimit      int           `envconfig:"RATE_LIMIT"`
	RequestTimeout time.Duration `envconfig:"REQUEST_TIMEOUT"`
}

// A hack for Heroku: their port env's name is $PORT and cannot be changed
func (c *Config) maybeLoadPort(log *logrus.Entry) {
	port := os.Getenv("PORT")
	if port == "" {
		return
	}

	portInt, err := strconv.Atoi(port)
	if err != nil {
		log.WithError(err).WithField("$PORT", port).Error("failed to parse $PORT, ignoring")
		return
	}

	log.Info("using port value from $PORT")

	c.Port = portInt
}

func readConfig(log *logrus.Entry) Config {
	var config Config

	err := envconfig.Process("ANAGRAM", &config)
	if err != nil {
		log.WithError(err).Fatal("failed to read config")
	}

	config.maybeLoadPort(log)

	return config
}
